<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Post;

class PostsControllerApi extends Controller
{
     // index page
     public function index() {
       
        $posts = Post::orderBy('id', 'desc')->paginate(5);
        $count = Post::count();
        $data = [
            'posts' => $posts,
            'count' => $count
        ];
        if($data){
            return response($data, 200);
        }else{
            return response('unknown error', 400);
        }
    }

    //show page 
    public function show($id) {

        $post = Post::find($id);
        if($post){
            return response($post, 200);
        }else{
            return response('not found', 404);
        }
    }

    //store post

    public function store(Request $request) {

        $validatoer = Validator::make($request->all(),[

            'title' =>  'required|max:200',
            'body' => 'required|max:500',
            'user_id' => 'required',
        ]);

        if($validatoer->fails()){
            return response($validatoer->errors(), 400);
        }

        $post = new Post() ;
        $post->title =  $request->title;
        $post->body =  $request->body;
        $post->user_id =  $request->user_id;

        $post->save();

        if($post){
            return response($post, 200);
        }else{
            return response('unknown error', 400);
        }
        
    }

    // update post form 

    public function update(Request $request, $id) {

        $validatoer = Validator::make($request->all(),[

            'title' =>  'required|max:200',
            'body' => 'required|max:500',
        ]);

        if($validatoer->fails()){
            return response($validatoer->errors(), 400);
        }

        $post = Post::find($id);

        if($post){
            
            $post->title = $request->title;
            $post->body = $request->body;
            $post->save();

            return response($post, 200);
        }else{
            return response('notfound', 404);
        }
    }

    //destroy post
    public function destroy($id) {

        $post = POST::find($id);
        if($post){
            $post->delete();
            return response('post was deleted', 200);
        }else{
            return response('notfound', 404);
        }
    }

}
