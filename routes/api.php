<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function(){
    /** Start post section  */

    // Show All Posts with paginate
    Route::get('index_posts', 'PostsControllerApi@index');
    // Show one Post with id
    Route::get('show_post/{id}', 'PostsControllerApi@show');
    // Store Post at Database
    Route::post('store_post', 'PostsControllerApi@store');
    //Update Post with id and data
    Route::put('update_post/{id}', 'PostsControllerApi@update');
    // Delete post with id
    Route::delete('destroy_post/{id}', 'PostsControllerApi@destroy');

    /** end post section  */

});
